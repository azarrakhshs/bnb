package com.example.bnbtest2;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class MainActivity2 extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private List<Object> viewItems = new ArrayList<>();

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private static final String TAG = "MainActivity";

    String coin="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        getSupportActionBar().hide();
        TextView mTextView = (TextView)findViewById(R.id.title2);
        if(getIntent().getExtras().containsKey("coin"))
            coin = getIntent().getExtras().getString("coin");
        mTextView.setText(coin);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);


        // use this setting to improve performance if you know that changes

        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        mAdapter = new RecyclerAdapter2(this, viewItems);
        mRecyclerView.setAdapter(mAdapter);

        addItemsFromJSON(coin);

        ImageView backBtn = (ImageView)findViewById(R.id.back);
        backBtn.setOnClickListener(view -> {
            this.finish();
        });


    }

    private void addItemsFromJSON(String coin) {
        try {

            String jsonDataString = readJSONDataFromFile();
            //JSONArray jsonArray = new JSONArray(jsonDataString);
            JSONObject main = new JSONObject(jsonDataString);
            JSONArray result = main.getJSONArray(coin);

            for (int i=0; i<result.length(); ++i) {

                JSONObject itemObj = result.getJSONObject(i);

                String size = itemObj.getString("size");
                String price = itemObj.getString("price");
                String side = itemObj.getString("side");
                String timestamp = itemObj.getString("timestamp");

                Trade myTrade = new Trade(size, price,side,timestamp);
                viewItems.add(myTrade);
            }

        } catch (JSONException | IOException e) {
            Log.d(TAG, "addItemsFromJSON: ", e);
        }
    }

    private String readJSONDataFromFile() throws IOException {

        InputStream inputStream = null;
        StringBuilder builder = new StringBuilder();

        try {

            String jsonString = null;
            inputStream = getResources().openRawResource(R.raw.trades);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, "UTF-8"));

            while ((jsonString = bufferedReader.readLine()) != null) {
                builder.append(jsonString);
            }

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return new String(builder);
    }

    public Trade parseObject(JSONObject TradeObject) throws JSONException {
        Trade trade = new Trade("","","","");
        trade.size = TradeObject.getString("size");
        trade.price = TradeObject.getString("price");
        trade.side = TradeObject.getString("side");
        trade.timestamp = TradeObject.getString("timestamp");

        return trade;
    }
}