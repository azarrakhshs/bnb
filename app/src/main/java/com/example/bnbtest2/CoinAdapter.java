package com.example.bnbtest2;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

class CoinAdapter extends RecyclerView.Adapter<CoinAdapter.ViewHolder> {
    private List<String> coin = new ArrayList<>();
    private final Context mContext;
    public CoinAdapter(Context mContext) {
        this.mContext = mContext;
    }
    View CurrentView;
    ViewGroup Parent;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Parent = parent;
        CurrentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.coin_item, parent, false);

        return new ViewHolder(CurrentView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.name.setText(coin.get(position));

        holder.cardView.setOnClickListener(v -> {
            Intent i = new Intent(mContext, MainActivity2.class);
            i.putExtra("coin",coin.get(position));
            mContext.startActivity(i);
        });

    }


    @Override
    public int getItemCount() {
        return coin.size();
    }

    public void setCoins(List<String> coin) {
        this.coin = coin;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        private TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.cardView);
            name = itemView.findViewById(R.id.name);

        }
    }

}