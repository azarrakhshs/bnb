package com.example.bnbtest2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private RecyclerView recView;
    private CoinAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new CoinAdapter(this);
        recView = findViewById(R.id.coin_recycler_view);
        recView.setAdapter(adapter);
        recView.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<String> coins = new ArrayList();
        coins.add("bnb-irt");
        coins.add("xrp-usdt");
        coins.add("shib-irt");
        coins.add("dot-irt");
        coins.add("btc-usdt");
        coins.add("usdt-irt");
        coins.add("btc-irt");
        coins.add("avax-irt");
        coins.add("eth-usdt");
        coins.add("doge-irt");
        coins.add("eth-irt");
        coins.add("xtz-irt");
        coins.add("bch-irt");
        coins.add("xlm-usdt");
        coins.add("trx-usdt");
        coins.add("ltc-irt");
        coins.add("link-irt");
        coins.add("bnb-usdt");
        coins.add("ada-usdt");
        coins.add("xrp-irt");
        coins.add("pmn-usdt");
        coins.add("matic-irt");
        coins.add("ada-irt");
        coins.add("sushi-irt");
        coins.add("trx-irt");
        coins.add("uni-irt");
        coins.add("doge-usdt");
        coins.add("xlm-irt");
        coins.add("sol-irt");
        coins.add("axs-irt");
        coins.add("sol-usdt");
        coins.add("xmr-irt");
        coins.add("dai-irt");

        adapter.setCoins(coins);
    }

}