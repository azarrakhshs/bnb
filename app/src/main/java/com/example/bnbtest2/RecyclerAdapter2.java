package com.example.bnbtest2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerAdapter2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE = 1;
    private final Context context;
    private final List<Object> listRecyclerItem;

    public RecyclerAdapter2(Context context, List<Object> listRecyclerItem) {
        this.context = context;
        this.listRecyclerItem = listRecyclerItem;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;
        private TextView size;
        private TextView price;
        private TextView side;
        private TextView timestamp;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            size = (TextView) itemView.findViewById(R.id.size);
            price = (TextView) itemView.findViewById(R.id.price);
            side = (TextView) itemView.findViewById(R.id.side);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        switch (i) {
            case TYPE:

            default:

                View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                        R.layout.list_item, viewGroup, false);

                return new ItemViewHolder((layoutView));
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        int viewType = getItemViewType(i);

        switch (viewType) {
            case TYPE:
            default:

                ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
                Trade Trade = (Trade) listRecyclerItem.get(i);

                itemViewHolder.size.setText(Trade.getSize());
                itemViewHolder.price.setText(Trade.getPrice());
                itemViewHolder.side.setText(Trade.getSide());
                itemViewHolder.timestamp.setText(Trade.getTimestamp());
//                itemViewHolder.cardView.setOnClickListener(v -> {
//                    itemViewHolder.cardView.setCardBackgroundColor(Color.RED);
//                });
        }

    }

    @Override
    public int getItemCount() {
        return listRecyclerItem.size();
    }
}