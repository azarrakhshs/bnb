package com.example.bnbtest2;

import java.io.Serializable;

public class Trade implements Serializable {
    public String size = "";
    public String price = "";
    public String side = "";
    public String timestamp = "";

    public Trade(String size, String price,String side,String timestamp) {
        this.size = size;
        this.price = price;
        this.side = side;
        this.timestamp = timestamp;
    }

    public String getSize() {
        return size;
    }

    public String getPrice() {
        return price;
    }
    public String getSide() {
        return side;
    }
    public String getTimestamp() {
        return timestamp;
    }

}
